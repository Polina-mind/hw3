const LoadStatusesEnum = {
  NEW: "NEW",
  POSTED: "POSTED",
  ASSIGNED: "ASSIGNED",
  SHIPPED: "SHIPPED",
};
Object.freeze(LoadStatusesEnum);

module.exports = { LoadStatusesEnum };
