const RolesEnum = { DRIVER: "DRIVER", SHIPPER: "SHIPPER" };
Object.freeze(RolesEnum);

module.exports = { RolesEnum };
