const express = require("express");
const router = express.Router();

const {
  getLoadsByUserId,
  addLoadToUser,
  getLoadByIdForUser,
  updateLoadByIdForUser,
  deleteLoadByIdForUser,
  getActiveLoadsByUserId,
  changeStateLoadsByUserId,
  postLoadByIdForUser,
} = require("../services/loadsService");

const {
  getAllAssignedTrucks,
  updateTruckByIdForUser,
} = require("../services/trucksService");

const { roleCheck } = require("../utils/roleCheck");
const { RolesEnum } = require("../enums/rolesEnum");
const { TruckTypesEnum, TruckStatusesEnum } = require("../enums/trucksEnum");

const { asyncWrapper } = require("../utils/apiUtils");
const { InvalidRequestError } = require("../utils/errors");

//getLoadsByUserId
router.get(
  "/",
  asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const { offset, limit } = req.query;

    // await roleCheck(userId, RolesEnum.SHIPPER);

    const loads = await getLoadsByUserId(userId, offset, limit);

    res.json({ loads });
  })
);

//getActiveLoadsByUserId
router.get(
  "/active",
  asyncWrapper(async (req, res) => {
    const { userId } = req.user;

    // await roleCheck(userId, RolesEnum.SHIPPER);

    const activeLoads = await getActiveLoadsByUserId(userId);

    res.json({ activeLoads });
  })
);

//addLoadToUser
router.post(
  "/",
  asyncWrapper(async (req, res) => {
    const { userId } = req.user;

    await roleCheck(userId, RolesEnum.SHIPPER);

    await addLoadToUser(userId, req.body);
    res.json({ message: "Load created successfully" });
  })
);

//getLoadsByIdForUser
router.get(
  "/:id",
  asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const { id } = req.params;

    const load = await getLoadByIdForUser(id);

    if (!load) {
      throw new InvalidRequestError("No load with such id found!");
    }

    // console.log(userId.includes(load.assigned_to));

    // if (!userId.includes(load.assigned_to)) {
    //   await roleCheck(userId, RolesEnum.SHIPPER);
    // } else {
    //   res.json({ load });
    // }

    res.json({ load });
  })
);

//updateLoadByIdForUser
router.put(
  "/:id",
  asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const { id } = req.params;
    const data = req.body;

    const load = await getLoadByIdForUser(id);

    if (!load) {
      throw new InvalidRequestError("No load with such id found!");
    }

    if (!userId.includes(load.assigned_to)) {
      await roleCheck(userId, RolesEnum.SHIPPER);
    } else {
      await updateLoadByIdForUser(id, data);

      res.json({ message: "Load details changed successfully" });
    }
  })
);

//deleteLoadByIdForUser
router.delete(
  "/:id",
  asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const { id } = req.params;

    await roleCheck(userId, RolesEnum.SHIPPER);

    await deleteLoadByIdForUser(id, userId);
    res.json({ message: "Load was deleted successful" });
  })
);

//changeStateLoadsByUserId
router.patch(
  "/active/state",
  asyncWrapper(async (req, res) => {
    const { userId } = req.user;

    // await roleCheck(userId, RolesEnum.SHIPPER);

    await changeStateLoadsByUserId(userId);

    res.json({ message: "Load state changed to 'En route to Delivery'" });
  })
);

// assignLoadByIdForUser
router.route("/:id/post").post(
  asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const { id } = req.params;

    // let driverFound = false;

    await roleCheck(userId, RolesEnum.SHIPPER);

    const load = await getLoadByIdForUser(id, userId);
    if (!load) {
      throw new InvalidRequestError("Load not found!");
    }

    await postLoadByIdForUser(id);

    // const trucks = await getAllAssignedTrucks(TruckStatusesEnum.IS);

    // console.log(trucks);

    // trucks.forEach((truck) => {
    //   if (!driverFound && truck.assigned_to === truck.userId) {
    //     driverFound = true;
    //     truck.status = TruckStatusesEnum.OL;
    //     updateTruckByIdForUser(truck.id, truck.userId, truck);

    //     load.status = LoadStatusesEnum.ASSIGNED;
    //     load.logs.push({
    //       message: `Load assigned to driver with id ${truck.assigned_to}`,
    //       time: Date.now(),
    //     });
    //     updateLoadByIdForUser(load.id, load);
    //   }
    // });

    res.json({
      message: "Load posted successfully",
      driver_found: true,
    });
  })
);

module.exports = {
  loadsRouter: router,
};
