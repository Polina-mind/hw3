const express = require("express");
const router = express.Router();

const { userValidationSchema } = require("../utils/userValidationSchema");

const { registration, login } = require("../services/authService");
const { InvalidRequestError } = require("../utils/errors");

const { asyncWrapper } = require("../utils/apiUtils");

router.post(
  "/register",
  asyncWrapper(async (req, res) => {
    const result = userValidationSchema.validate(req.body, {
      abortEarly: false,
    });
    console.log(result);

    if (result.error) {
      throw new InvalidRequestError(result.error);
    }

    const { email, password, role } = req.body;

    const roleUpperCased = role.toUpperCase();

    await registration({ email, password, role: roleUpperCased });

    res.json({ message: "Account created successfully!" });
  })
);

router.post(
  "/login",
  asyncWrapper(async (req, res) => {
    const { email, password } = req.body;

    const token = await login({ email, password });
    // console.log(token);

    res.json({ jwt_token: token });
  })
);

//forgotPassword
router.post(
  "/forgot_password",
  asyncWrapper(async (req, res) => {
    // const { email } = req.body;

    res.json({ message: "New password sent to your email address!" });
  })
);

module.exports = {
  authRouter: router,
};
