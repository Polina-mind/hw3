const mongoose = require("mongoose");
const { TruckTypesEnum, TruckStatusesEnum } = require("../enums/trucksEnum");

const Truck = mongoose.model("Truck", {
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },

  created_by: mongoose.Schema.Types.ObjectId,

  assigned_to: String,

  status: {
    type: TruckStatusesEnum,
    default: "IS",
  },

  type: {
    type: TruckTypesEnum,
    required: true,
  },

  createdAt: {
    type: Date,
    default: Date.now(),
  },

  __v: { type: Number, select: false },
});

module.exports = { Truck };
