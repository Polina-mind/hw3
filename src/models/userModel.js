const mongoose = require("mongoose");
const { RolesEnum } = require("../enums/rolesEnum");

const User = mongoose.model("User", {
  email: {
    type: String,
    required: true,
    unique: true,
  },

  password: {
    type: String,
    required: true,
    // select: false,
  },

  role: {
    type: RolesEnum,
    required: true,
  },

  createdAt: {
    type: Date,
    default: Date.now(),
  },

  __v: { type: Number, select: false },
});

module.exports = { User };
