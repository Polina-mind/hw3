const express = require("express");
const path = require("path");
const morgan = require("morgan");
const mongoose = require("mongoose");
const env = require("dotenv").config();

const port = process.env.PORT || 8080;
const app = express();

const { trucksRouter } = require("./controllers/trucksController");
const { authRouter } = require("./controllers/authController");
const { userRouter } = require("./controllers/userController");
const { loadsRouter } = require("./controllers/loadsController");
const { authMiddleware } = require("./middlewares/authMiddleware");
const { NodeCourseError } = require("./utils/errors");

app.use(express.json());
app.use(morgan("tiny"));

app.use("/api/auth", authRouter);

app.use("/api/trucks", [authMiddleware], trucksRouter);
app.use("/api/users/me", [authMiddleware], userRouter);
app.use("/api/loads", [authMiddleware], loadsRouter);

app.use((req, res, next) => {
  res.status(404).json({ message: "Not found" });
});

app.use((err, req, res, next) => {
  if (err instanceof NodeCourseError) {
    return res.status(err.status).json({ message: err.message });
  }
  res.status(500).json({ message: err.message });
});

const start = async () => {
  try {
    await mongoose.connect(
      "mongodb+srv://Polina:Polina1510@cluster0.kn7xm.mongodb.net/uber?retryWrites=true&w=majority",
      {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true,
        useFindAndModify: false,
      }
    );

    app.listen(port);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();
