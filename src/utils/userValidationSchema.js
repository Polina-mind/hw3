const Joi = require("joi");

const { RolesEnum } = require("../enums/rolesEnum");

const userValidationSchema = Joi.object().keys({
  role: Joi.string()
    .uppercase()
    .valid(...Object.values(RolesEnum)),
  email: Joi.string().email({
    minDomainSegments: 2,
    tlds: { allow: ["com", "net"] },
  }),
  password: Joi.string().pattern(new RegExp("^[a-zA-Z0-9]{3,30}$")),
});

// const driverRoleSchema = Joi.string().uppercase().valid(RolesEnum.DRIVER);

// const shipperRoleSchema = Joi.string().uppercase().valid(RolesEnum.SHIPPER);

module.exports = { userValidationSchema };
