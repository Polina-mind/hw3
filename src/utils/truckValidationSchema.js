const Joi = require("joi");
const { TruckTypesEnum, TruckStatusesEnum } = require("../enums/trucksEnum");

const truckTypesValidationSchema = Joi.object().keys({
  type: Joi.string()
    .uppercase()
    .valid(...Object.values(TruckTypesEnum)),
});

const truckStatusesValidationSchema = Joi.object().keys({
  type: Joi.string()
    .uppercase()
    .valid(...Object.values(TruckStatusesEnum)),
});

// const truckTypeSprinterSchema = Joi.string()
//   .uppercase()
//   .valid(TruckTypesEnum.SPRINTER);

// const truckTypeSmallSchema = Joi.string()
//   .uppercase()
//   .valid(TruckTypesEnum.SMALL_STRAIGHT);

// const truckTypeLargeSchema = Joi.string()
//   .uppercase()
//   .valid(TruckTypesEnum.LARGE_STRAIGHT);

// const truckStatusIsSchema = Joi.string()
//   .uppercase()
//   .valid(TruckStatusesEnum.IS);

// const truckStatusOlSchema = Joi.string()
//   .uppercase()
//   .valid(TruckStatusesEnum.OL);

module.exports = {
  truckTypesValidationSchema,
  truckStatusesValidationSchema,
  // truckTypeSprinterSchema,
  // truckTypeSmallSchema,
  // truckTypeLargeSchema,
  // truckStatusIsSchema,
  // truckStatusOlSchema,
};
