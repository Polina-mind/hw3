const { Truck } = require("../models/truckModel");

const getTrucksByUserId = async (userId, offset, limit) => {
  offset = parseInt(offset) || 0;
  limit = parseInt(limit) || 5;

  const trucks = await Truck.find({ userId }).skip(offset).limit(limit);

  return trucks;
};

const getAllTrucksByUserId = async (userId) => {
  const trucks = await Truck.find({ userId });

  return trucks;
};

const getTruckByIdForUser = async (truckId, userId) => {
  const truck = await Truck.findOne({ _id: truckId, userId });
  return truck;
};

const addTruckToUser = async (userId, truckPayload) => {
  const truck = new Truck({ ...truckPayload, userId });
  await truck.save();
};

const updateTruckByIdForUser = async (truckId, userId, data) => {
  await Truck.findOneAndUpdate({ _id: truckId, userId }, { $set: data });
};

const deleteTruckByIdForUser = async (truckId, userId) => {
  await Truck.findOneAndRemove({ _id: truckId, userId });
};

const assignTruckByIdForUser = async (id, userId) => {
  const truck = await getTruckByIdForUser(id, userId);

  truck.assigned_to = userId;
  return await Truck.findOneAndUpdate({ _id: id, userId }, { $set: truck });
};

const getAllAssignedTrucks = async (status) => {
  return await Truck.find({ status });
};

module.exports = {
  getTrucksByUserId,
  addTruckToUser,
  getTruckByIdForUser,
  assignTruckByIdForUser,
  deleteTruckByIdForUser,
  updateTruckByIdForUser,
  getAllTrucksByUserId,
  getAllAssignedTrucks,
};
