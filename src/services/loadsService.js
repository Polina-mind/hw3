const { Load } = require("../models/loadModel");

const getLoadsByUserId = async (userId, offset, limit) => {
  offset = parseInt(offset) || 0;
  limit = parseInt(limit) || 5;

  const loads = await Load.find({ userId }).skip(offset).limit(limit);

  return loads;
};

const getLoadByIdForUser = async (loadId) => {
  const load = await Load.findOne({ _id: loadId });
  return load;
};

const addLoadToUser = async (userId, loadPayload) => {
  const load = new Load({ ...loadPayload, userId, created_by: userId });
  await load.save();
};

const updateLoadByIdForUser = async (loadId, data) => {
  await Load.findOneAndUpdate({ _id: loadId }, { $set: data });
};

const deleteLoadByIdForUser = async (loadId, userId) => {
  await Load.findOneAndRemove({ _id: loadId, userId });
};

const getActiveLoadsByUserId = async (userId) => {
  const loads = await Load.find({ userId });

  const activeLoads = loads.map((load) => {
    if (load.driver_found === true) {
      return load;
    }
  });

  return {
    activeLoads,
  };
};

const changeStateLoadsByUserId = async (userId) => {
  await Load.findOneAndUpdate(
    { userId },
    { $set: { state: "En route to Delivery" } }
  );
};

const postLoadByIdForUser = async (_id) => {
  await Load.findOneAndUpdate(
    { _id },
    { $set: { message: "Load posted successfully", driver_found: true, status: "SHIPPED" } }
  );
};

module.exports = {
  getLoadsByUserId,
  addLoadToUser,
  getLoadByIdForUser,
  updateLoadByIdForUser,
  deleteLoadByIdForUser,
  getActiveLoadsByUserId,
  changeStateLoadsByUserId,
  postLoadByIdForUser,
};
